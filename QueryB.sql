﻿/*
a. Написать процедуру, которая возвращает самый крупный заказ для каждого из продавцов за определенный год. 
В результатах не может быть несколько заказов одного продавца, должен быть только один и самый крупный. 
В результатах запроса должны быть выведены следующие колонки: колонка с именем и фамилией продавца 
(FirstName и LastName – пример: Nancy Davolio), номер заказа и его стоимость. 
В запросе надо учитывать Discount при продаже товаров. 
Процедуре передается год, за который надо сделать отчет, и количество возвращаемых записей.
Результаты запроса должны быть упорядочены по убыванию суммы заказа. 
Процедура должна быть реализована 2-мя способами: с использованием оператора SELECT и с использованием курсора. 
Название функций соответственно GreatestOrders и GreatestOrdersCur. 
Необходимо продемонстрировать использование этих процедур. 

Также помимо демонстрации вызовов процедур в скрипте QueryB.sql надо написать отдельный ДОПОЛНИТЕЛЬНЫЙ проверочный запрос для 
тестирования правильности работы процедуры GreatestOrders. 
Проверочный запрос должен выводить в удобном для сравнения с результатами работы процедур виде для определенного продавца 
для всех его заказов за определенный указанный год в результатах следующие колонки: имя продавца, номер заказа, сумму заказа. 
Проверочный запрос не должен повторять запрос, написанный в процедуре, - он должен выполнять только то, что описано 
в требованиях по нему.
*/
	CREATE PROCEDURE GreatestOrders
		@Year SMALLINT,
		@RecordCount SMALLINT
	AS
	BEGIN
		SELECT CONCAT(FirstName, ' ', LastName) AS 'SELLER', OrderID, MaxSumma
		  FROM
		  (
			SELECT TOP(@RecordCount) MAX(OrderSum.Summa) MaxSumma, OrderSum.EmployeeID, OrderSum.OrderID
			FROM
			(
				SELECT SUM(CONVERT(MONEY, ((UnitPrice - Discount) * Quantity), 1)) AS Summa, O.OrderID, O.EmployeeID, 
				ROW_NUMBER() OVER (PARTITION BY O.EmployeeID 
				ORDER BY SUM(CONVERT(MONEY, ((UnitPrice - Discount) * Quantity), 1)) DESC) NUM
				FROM [Order Details] OD
				JOIN Orders O 
				ON OD.OrderID = O.OrderID 
				WHERE YEAR(OrderDate) = @Year
				GROUP BY O.EmployeeID, O.OrderID
			) OrderSum
			WHERE NUM = 1
			GROUP BY OrderSum.EmployeeID, OrderSum.OrderID
			ORDER BY MaxSumma DESC
		  ) OrderSumMax
		  JOIN Employees E 
			ON OrderSumMax.EmployeeID = E.EmployeeID
	END 
	GO

	CREATE PROCEDURE GreatestOrdersCur
		@Year SMALLINT,
		@RecordCount SMALLINT
	AS
	BEGIN
		DECLARE @employeeID INT;
		DECLARE employeeCursor CURSOR FOR
		SELECT EmployeeID
		FROM Employees

		DECLARE @MAXORDERS TABLE
		(
			EmployeeId INT,
			OrderId INT,
			MaxSumma MONEY
		)

		OPEN employeeCursor

		FETCH NEXT FROM employeeCursor
		INTO @employeeID 
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO @MAXORDERS (EmployeeId, OrderId, MaxSumma) 
				SELECT TOP(1) O.EmployeeID, O.OrderID, SUM(CONVERT(MONEY, ((UnitPrice - Discount) * Quantity), 1)) SUMMA
				FROM [Order Details] OD
				JOIN Orders O 
				ON OD.OrderID = O.OrderID 
				WHERE YEAR(OrderDate) = @Year AND O.EmployeeID = @employeeID
				GROUP BY O.EmployeeID, O.OrderID
				ORDER BY SUMMA DESC

			FETCH NEXT FROM employeeCursor
			INTO @employeeID

		END
		CLOSE employeeCursor
		DEALLOCATE employeeCursor

		SELECT TOP(@RecordCount) CONCAT(FirstName, ' ', LastName) AS 'SELLER', OrderID, MaxSumma
		FROM @MAXORDERS M
		JOIN Employees E
		ON M.EmployeeId = E.EmployeeID
		ORDER BY MaxSumma DESC
	END 
	GO

	-- CHECKING
	EXEC GreatestOrders 1997, 5;
	EXEC GreatestOrdersCur 1997, 5

	SELECT CONCAT(FirstName, ' ', LastName) AS 'SELLER', O.OrderID, SUM(CONVERT(MONEY, ((UnitPrice - Discount) * Quantity), 1)) SUMMA
	FROM Employees E 
	JOIN Orders O	
	ON E.EmployeeID = O.EmployeeID
	JOIN [Order Details] OD
	ON O.OrderID = OD.OrderID
	WHERE YEAR(OrderDate) = 1997 AND FirstName = 'Robert' AND LastName = 'King'
	GROUP BY FirstName, LastName, O.OrderID
	ORDER BY SUMMA DESC
	GO
/*
b. Написать процедуру, которая возвращает заказы в таблице Orders, согласно указанному сроку доставки в днях 
(разница между OrderDate и ShippedDate).  В результатах должны быть возвращены заказы, срок которых превышает переданное 
значение, или еще недоставленные заказы. Значению по умолчанию для передаваемого срока 35 дней. Название процедуры 
ShippedOrdersDiff. Процедура должна высвечивать следующие колонки: OrderID, OrderDate, ShippedDate, ShippedDelay 
(разность в днях между ShippedDate и OrderDate), SpecifiedDelay (переданное в процедуру значение).  
Необходимо продемонстрировать использование этой процедуры.
*/

	CREATE OR ALTER PROCEDURE ShippedOrdersDiff
		@DaysLimit INT = 35
	AS 
	BEGIN
		SELECT OrderID, OrderDate, ShippedDate,  DATEDIFF(DAY, OrderDate, ShippedDate) AS ShippedDelay, @DaysLimit AS SpecifiedDelay
		FROM Orders
		WHERE ShippedDate IS NULL OR ShippedDate - OrderDate > @DaysLimit
	END
	GO

	-- CHECKING
	EXEC ShippedOrdersDiff
	EXEC ShippedOrdersDiff 10
	GO

/*
c. Написать процедуру, которая высвечивает всех подчиненных заданного продавца, как непосредственных, так и подчиненных 
его подчиненных. В качестве входного параметра функции используется EmployeeID. Необходимо распечатать имена подчиненных 
и выровнять их в тексте (использовать оператор PRINT) согласно иерархии подчинения. Продавец, для которого надо найти 
подчиненных, также должен быть высвечен. Название процедуры SubordinationInfo. В качестве алгоритма для решения этой 
задачи надо использовать пример, приведенный в Books Online и рекомендованный Microsoft для решения подобного типа задач. 
Продемонстрировать использование процедуры.
*/
	CREATE OR ALTER PROCEDURE SubordinationInfo
		@EmployeeId INT
	AS
	BEGIN
		DECLARE @Names TABLE
		(
			Name VARCHAR(255)
		);

		INSERT INTO @Names (Name)
		SELECT CONCAT(FirstName, ' ', LastName) 
		FROM Employees
		WHERE EmployeeID = @EmployeeId;

		WITH EmployeeReports (ReportsTo, EmployeeId, Name, Level, Sort)
		AS
		(
			SELECT E.ReportsTo, E.EmployeeID, 
				CONVERT(VARCHAR(255), E.FirstName + ' ' + E.LastName), 1,
				CONVERT(VARCHAR(255), E.FirstName + ' ' + E.LastName)
			FROM Employees E
			WHERE EmployeeID = @EmployeeId
			UNION ALL
			SELECT E.ReportsTo, E.EmployeeID,
			CONVERT(VARCHAR(255), REPLICATE ('|      ', Level) + E.FirstName + ' ' + E.LastName), Level + 1,
			CONVERT(VARCHAR(255), RTRIM (Sort) + '|      ' + E.FirstName + ' ' + E.LastName)
			FROM Employees E
			INNER JOIN EmployeeReports ER
			ON E.ReportsTo = ER.EmployeeId
		)

		INSERT INTO @Names
		SELECT Name
		FROM EmployeeReports
		WHERE Level <= 3 AND Level > 1
		ORDER BY Sort

		DECLARE @Name VARCHAR(255);
		DECLARE NameCur CURSOR FOR
		SELECT Name 
		FROM @Names
	
		OPEN NameCur

		FETCH NEXT FROM NameCur
		INTO @Name

		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT @Name
			FETCH NEXT FROM NameCur
			INTO @Name
		END
		CLOSE NameCur;
		DEALLOCATE NameCur;
	END

	--CHECKING
	EXEC SubordinationInfo 1
	EXEC SubordinationInfo 2
	EXEC SubordinationInfo 5
/*
d. Написать функцию, которая определяет, есть ли у продавца подчиненные. Возвращает тип данных BIT. В качестве входного 
параметра функции используется EmployeeID. Название функции IsBoss. Продемонстрировать использование функции для всех 
продавцов из таблицы Employees.
*/
