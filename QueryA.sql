﻿/*
1. Работа с типами данных Date, NULL значениями, трехзначная логика. Возвращение определенных значений в результатах запроса в зависимости от 
полученных первоначальных значений результата запроса. Высветка в результатах запроса только определенных колонок.

	a. Выбрать в таблице Orders заказы, которые были доставлены после 6 мая 1998 года (колонка ShippedDate) включительно и которые доставлены с 
	ShipVia >= 2. Формат указания даты должен быть верным при любых региональных настройках, согласно требованиям статьи “Writing International 
	Transact-SQL Statements” в Books Online раздел “Accessing and Changing Relational Data Overview”. Этот метод использовать далее для всех заданий. 
	Запрос должен высвечивать только колонки OrderID, ShippedDate и ShipVia.
	Пояснить почему сюда не попали заказы с NULL-ом в колонке ShippedDate.
*/
	SELECT OrderID, ShippedDate, ShipVia
	  FROM Orders
	 WHERE ShippedDate >= '1998-05-06'
	   AND ShipVia >=2
	 -- Не попали заказы с NULL-ом в колонке ShippedDate, т.к. в результате фильтрации останутся строки, для которых условие в разделе WHERE возвращает
	 -- TRUE. Для строк с NULL-ом условие возвращает UNKNOWN.
/*
	b. Написать запрос, который выводит только недоставленные заказы из таблицы Orders. В результатах запроса высвечивать для колонки ShippedDate 
	вместо значений NULL строку ‘Not Shipped’ – использовать системную функцию CASЕ. Запрос должен высвечивать только колонки OrderID и ShippedDate.
*/
	SELECT OrderID, 
	  CASE 
		WHEN ShippedDate IS NULL THEN 'Not Shipped'
	   END
	  FROM Orders
	 WHERE ShippedDate IS NULL
/*
	c. Выбрать в таблице Orders заказы, которые были доставлены после 6 мая 1998 года (ShippedDate), не включая эту дату, или которые еще не доставлены. 
	В запросе должны высвечиваться только колонки OrderID (переименовать в Order Number) и ShippedDate (переименовать в Shipped Date). 
	В результатах запроса высвечивать для колонки ShippedDate вместо значений NULL строку ‘Not Shipped’, для остальных значений высвечивать дату 
	в формате по умолчанию.
*/
	SELECT OrderID AS "Order Number",
	  CASE 
		WHEN ShippedDate IS NULL THEN 'Not Shipped'
		ELSE CAST (ShippedDate AS NVARCHAR(15))
	   END
	    AS "Shipped Date"
	  FROM Orders
	 WHERE ShippedDate > '1998-05-06'
	    OR ShippedDate IS NULL
/*
2. Использование операторов IN, DISTINCT, ORDER BY, NOT
	a. Выбрать из таблицы Customers всех заказчиков, проживающих в USA и Canada. Запрос сделать только с помощью оператора IN. Высвечивать колонки с 
	именем пользователя и названием страны в результатах запроса. Упорядочить результаты запроса по имени заказчиков и по месту проживания.
*/
	SELECT ContactName, Country
	  FROM Customers
	 WHERE Country IN ('USA', 'Canada')
  ORDER BY ContactName, Country
/*
	b. Выбрать из таблицы Customers всех заказчиков, не проживающих в USA и Canada. Запрос сделать с помощью оператора IN. Высвечивать колонки с 
	именем пользователя и названием страны в результатах запроса. Упорядочить результаты запроса по имени заказчиков.
*/
	SELECT ContactName, Country
	  FROM Customers
	 WHERE Country NOT IN ('USA', 'Canada')
  ORDER BY ContactName
/*
	c. Выбрать из таблицы Customers все страны, в которых проживают заказчики. Страна должна быть упомянута только один раз, и список отсортирован по 
	убыванию. Не использовать предложение GROUP BY. Высвечивать только одну колонку в результатах запроса.
*/
	SELECT DISTINCT Country
	  FROM Customers
/*
3. Использование оператора BETWEEN, DISTINCT
	a. Выбрать все заказы (OrderID) из таблицы Order Details (заказы не должны повторяться), где встречаются продукты с количеством от 3 до 10 включительно – это колонка Quantity в таблице Order Details. 
	Использовать оператор BETWEEN. Запрос должен высвечивать только колонку OrderID.
*/
	SELECT DISTINCT OrderID
	  FROM [Order Details]
	 WHERE Quantity BETWEEN 3 AND 10
/*
	b. Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g. Использовать оператор BETWEEN. Проверить, что в результаты запроса попадает Germany. 
	Запрос должен высвечивать только колонки CustomerID и Country и отсортирован по Country.
*/
	SELECT CustomerID, Country
	  FROM Customers
	 WHERE left(Country, 1) BETWEEN 'b' AND 'g'
  ORDER BY Country
/*
	c. Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g, не используя оператор BETWEEN. 
	С помощью опции “Execution Plan” определить какой запрос предпочтительнее 3.2 или 3.3 – для этого надо ввести в скрипт выполнение текстового Execution Plan-a для двух этих запросов, 
	результаты выполнения Execution Plan надо ввести в скрипт в виде комментария и по их результатам дать ответ на вопрос – по какому параметру было проведено сравнение. 
	Запрос должен высвечивать только колонки CustomerID и Country и отсортирован по Country.
*/
	SELECT CustomerID, Country
	  FROM Customers
	 WHERE left(Country, 1) IN ('b', 'c', 'd', 'e', 'f', 'g')
  ORDER BY Country

  --Ответ: при данном наборе данных разницы не обнаружено. Скрипты хранятся в файлах 3B и 3C соответственно
/*
4. Использование оператора LIKE
	a. В таблице Products найти все продукты (колонка ProductName), где встречается подстрока 'chocolade'. 
	Известно, что в подстроке 'chocolade' может быть изменена одна буква 'c' в середине - найти все продукты, которые удовлетворяют этому условию. 
	Подсказка: результаты запроса должны высвечивать 2 строки.
*/
	SELECT ProductName
	  FROM Products
	 WHERE ProductName LIKE N'%cho_olade%'
/*
5. Использование агрегатных функций (SUM, COUNT)
	a. Найти общую сумму всех заказов из таблицы Order Details с учетом количества закупленных товаров и скидок по ним. Результат округлить до сотых и высветить в стиле 1 для типа данных money. 
	Скидка (колонка Discount) составляет процент из стоимости для данного товара. Для определения действительной цены на проданный продукт надо вычесть скидку из указанной в колонке UnitPrice цены. 
	Результатом запроса должна быть одна запись с одной колонкой с названием колонки 'Totals'.
*/
	SELECT CONVERT(MONEY, SUM((UnitPrice - Discount) * Quantity), 1) AS 'Totals'
	  FROM [Order Details]
/*
	b. По таблице Orders найти количество заказов, которые еще не были доставлены (т.е. в колонке ShippedDate нет значения даты доставки). Использовать при этом запросе только оператор COUNT. 
	Не использовать предложения WHERE и GROUP.
		Несколько вариантов (прежде чем смотреть подсказки, сделайте своё решение):
			Можно использовать CASE, чтобы «преобразовать» заполненность ShippedDate в 1 или NULL.
			Можно посчитать количество всех заказов и количество заказов, у которых не заполнено ShippedDate.
*/
	SELECT COUNT(OrderID) - COUNT(ShippedDate)
	  FROM Orders
/*
	c. По таблице Orders найти количество различных покупателей (CustomerID), сделавших заказы. Использовать функцию COUNT и не использовать предложения WHERE и GROUP.
*/
	SELECT COUNT(DISTINCT CustomerID)
	  FROM Orders
/*
6. Явное соединение таблиц, самосоединения, использование агрегатных функций и предложений GROUP BY и HAVING
	a. По таблице Orders найти количество заказов с группировкой по годам. В результатах запроса надо высвечивать две колонки c названиями Year и Total.
	Написать проверочный запрос, который вычисляет количество всех заказов.
*/
	SELECT YEAR(OrderDate) AS 'Year', COUNT(OrderID) AS 'Total'
	  FROM Orders
  GROUP BY YEAR(OrderDate)
  
  --CHECKING QUERY
    SELECT COUNT(OrderID) AS 'Total'
	  FROM Orders
/*
	b. По таблице Orders найти количество заказов, cделанных каждым продавцом. Заказ для указанного продавца – это любая запись в таблице Orders, 
	где в колонке EmployeeID задано значение для данного продавца. В результатах запроса надо высвечивать колонку с именем продавца 
	(Должно высвечиваться имя полученное конкатенацией LastName & FirstName. Эта строка LastName & FirstName должна быть получена отдельным запросом в колонке основного запроса. 
	Также основной запрос должен использовать группировку по EmployeeID.) с названием колонки ‘Seller’ и колонку c количеством заказов высвечивать с названием 'Amount'. 
	Результаты запроса должны быть упорядочены по убыванию количества заказов.
*/
  SELECT 
		(SELECT CONCAT(LastName, ' ', FirstName) 
		   FROM Employees E
		  WHERE E.EmployeeID = O.EmployeeID)
		     AS 'Seller',
		COUNT(OrderID)
		     AS 'Amount'
	  FROM Orders O
  GROUP BY EmployeeID
  ORDER BY 'Amount' DESC
	  
/*
	c*. По таблице Orders найти количество заказов, cделанных каждым продавцом и для каждого покупателя. Необходимо определить это только для заказов сделанных в 1998 году.
	В результатах запроса надо высвечивать колонку с именем продавца (название колонки ‘Seller’), колонку с именем покупателя (название колонки ‘Customer’)
	и колонку c количеством заказов высвечивать с названием 'Amount'. В запросе необходимо использовать специальный оператор языка T-SQL для работы с выражением GROUP 
	(Этот же оператор поможет выводить строку “ALL” в результатах запроса). Группировки должны быть сделаны по ID продавца и покупателя. 
	Результаты запроса должны быть упорядочены по продавцу, покупателю и по убыванию количества продаж. В результатах должна быть сводная информация по продажам.
	Т.е. в результирующем наборе должны присутствовать дополнительно к информации о продажах продавца для каждого покупателя следующие строчки:
		Seller                         Customer                             Amount
		ALL                            ALL                                       <общее число продаж>
		<имя>                      ALL                                       <число продаж для данного продавца>
		ALL                           <имя>                                  <число продаж для данного покупателя>
		<имя>                     <имя>                                  <число продаж данного продавца для данного покупателя>
*/
	SELECT 
		ISNULL(NULLIF(CONCAT(FirstName, ' ', LastName), ''), 'ALL') AS 'Seller',
		ISNULL(NULLIF(ContactName, ''), 'ALL') AS 'Customer',
		COUNT(OrderID) AS 'Amount'
	  FROM Employees E FULL JOIN Orders O ON E.EmployeeID = O.EmployeeID FULL JOIN Customers C ON O.CustomerID = C.CustomerID
	 WHERE YEAR(OrderDate) = 1998
	 GROUP BY
		GROUPING SETS
		(
			(),
			(E.FirstName, E.LastName),
			(C.ContactName),
			(E.FirstName, E.LastName, C.ContactName)			
		)
	ORDER BY C.ContactName, Seller, Amount DESC
/*
	d. Найти покупателей и продавцов, которые живут в одном городе. Если в городе живут только один или несколько продавцов или только один или несколько покупателей, 
	то информация о таких покупателя и продавцах не должна попадать в результирующий набор. Не использовать конструкцию JOIN. В результатах запроса необходимо вывести следующие заголовки 
	для результатов запроса: ‘Person’, ‘Type’ (здесь надо выводить строку ‘Customer’ или  ‘Seller’ в зависимости от типа записи), ‘City’. Отсортировать результаты запроса по колонке ‘City’ и по ‘Person’.
		Прежде чем смотреть подсказки, сделайте своё решение:
			Тут надо выбрать только тех покупателей, которые живут в городах, в которых есть «продавцы».
			Потом выбрать только тех «продавцов», которые живут в городах, в которых есть покупатели.
			И объединить два результата с сортировкой.
*/
	SELECT Person, Type, City 
	  FROM
		(SELECT DISTINCT ContactName 
		     AS 'Person', 'Customer' AS 'Type', City
		   FROM Customers C
		  WHERE City IN 
			(SELECT City 
			   FROM Employees)
		  UNION
		 SELECT DISTINCT CONCAT(FirstName, ' ', LastName) AS 'Person', 
			'Seller' AS 'Type', 
			City
		   FROM Employees E
		  WHERE City IN
			(SELECT City
			   FROM Customers)) PER
	ORDER BY PER.City, PER.Person

/*	e. Найти всех покупателей, для которых есть другие покупатели, живующие в том же городе. В запросе использовать соединение таблицы Customers c собой - самосоединение. 
	Высветить колонки CustomerID и City. Запрос не должен высвечивать дублируемые записи. Для проверки написать запрос, который высвечивает города, которые встречаются более одного раза в таблице Customers. 
	Это позволит проверить правильность запроса.
*/
	SELECT DISTINCT C1.CustomerID, C1.City
	  FROM Customers C1 CROSS JOIN Customers C2
	 WHERE C1.City = C2.City AND C1.CustomerID <> C2.CustomerID

	-- CHECKING QUERY
	SELECT DISTINCT City,  COUNT(CustomerID)
	  FROM Customers
	GROUP BY City
	HAVING COUNT(CustomerID) > 1
/*
	f. По таблице Employees найти для каждого продавца его руководителя, т.е. кому он делает репорты. Высветить колонки с именами 'User Name' (LastName) и 'Boss'. 
	В колонках должны быть высвечены имена из колонки LastName.
*/
	SELECT E1.LastName AS 'User Name', E2.LastName AS 'Boss'
	  FROM Employees E1 LEFT JOIN Employees E2 
	    ON E1.ReportsTo = E2.EmployeeID
/*i. Ответьте на вопрос "Высвечены ли все продавцы в этом запросе?"
		Для этого на основе вышеуказанного запроса сделайте запрос, который выводит всех продавцов, которые не отчитываются.
		ii. Используйте оператор EXCEPT
*/
	SELECT LastName AS 'User Name'
	  FROM Employees
	EXCEPT 
	SELECT LastName AS 'User Name'
	  FROM Employees
	 WHERE ReportsTo IN 
		(SELECT EmployeeID
		   FROM Employees)
	-- Ответ: высвечены все продавцы, так как в запросе использован LEFT JOIN
/*
7. Использование Inner JOIN
	a*. Определить продавцов, которые обслуживают регион 'Western' (таблица Region). 
	Результаты запроса должны высвечивать два поля: 'LastName' продавца и название обслуживаемой территории 
	('TerritoryDescription' из таблицы Territories). Запрос должен использовать JOIN в предложении FROM. 
	Для определения связей между таблицами Employees и Territories надо использовать 
	графические диаграммы для базы Northwind.
*/
	SELECT E.LastName, T.TerritoryDescription
	  FROM Employees E 
		JOIN EmployeeTerritories ET ON E.EmployeeID = ET.EmployeeID
		JOIN Territories T ON ET.TerritoryID = T.TerritoryID
		JOIN Region R ON T.RegionID = R.RegionID
	 WHERE R.RegionDescription = 'Western'
/*
8. Использование Outer JOIN
	a. Высветить в результатах запроса имена всех заказчиков из таблицы Customers и суммарное количество их заказов из таблицы Orders. Принять во внимание, что у некоторых заказчиков нет заказов, 
	но они также должны быть выведены в результатах запроса. Упорядочить результаты запроса по возрастанию количества заказов.
*/
	SELECT C.ContactName, COUNT(O.OrderID) CNT
	  FROM Customers C LEFT JOIN Orders O 
	    ON C.CustomerID = O.CustomerID
  GROUP BY C.ContactName
  ORDER BY CNT
/*
9. Использование подзапросов
	a. Высветить всех поставщиков колонка CompanyName в таблице Suppliers, у которых нет хотя бы одного продукта на складе (UnitsInStock в таблице Products равно 0). 
	Использовать вложенный SELECT для этого запроса с использованием оператора IN. Можно ли использовать вместо оператора IN оператор '=' ?
*/
	SELECT CompanyName
	  FROM Suppliers
	 WHERE SupplierID IN
		(SELECT SupplierID
		   FROM Products
		  WHERE UnitsInStock = 0)
	-- Ответ: использовать оператор '=' нельзя, так как подзапрос возвращает более 1 значения
/*
10. Коррелированный запрос
	a*. Высветить всех продавцов, которые имеют более 150 заказов. Использовать вложенный коррелированный SELECT.
*/
	SELECT LastName
	  FROM Employees E
	 WHERE
		(SELECT COUNT(OrderID)
		   FROM Orders
		  WHERE EmployeeID = E.EmployeeID) > 150
/*11. Использование EXISTS
	a. Высветить всех заказчиков (таблица Customers), которые не имеют ни одного заказа (подзапрос по таблице Orders). 
	Использовать коррелированный SELECT и оператор EXISTS.
*/
	SELECT ContactName
	  FROM Customers C
	 WHERE NOT EXISTS
		(SELECT OrderID
		   FROM Orders
		  WHERE CustomerID = C.CustomerID)
/*
12. Использование строковых функций
	a. Для формирования алфавитного указателя Employees высветить из таблицы Employees список только тех букв алфавита, с которых начинаются фамилии Employees 
	(колонка LastName ) из этой таблицы. Алфавитный список должен быть отсортирован по возрастанию.
*/
	SELECT DISTINCT LEFT(LastName, 1) 'Letter'
	  FROM Employees
  ORDER BY Letter